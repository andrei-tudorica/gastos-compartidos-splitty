import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import MainView from '../../src/components/MainView';
import { BrowserRouter as Router } from 'react-router-dom';

describe('Mainview', () => {
  test('renders', () => {
    const { container } = render(
      <Router>
        <MainView>
          <p></p>
        </MainView>
      </Router>
    );

    expect(container).toBeInTheDocument();
  });

  test('children renders', async () => {
    render(
      <Router>
        <MainView>
          <p>Piso</p>
        </MainView>
      </Router>
    );

    const text = screen.queryByText('Piso');

    expect(text).toBeInTheDocument();
    expect(text).toBeVisible();
  });
});
