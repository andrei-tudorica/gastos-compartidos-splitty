import { DateTime } from 'luxon';
import { Group } from './../../src/models/Group.model.';
import { balanceExpenses } from '../../src/services/calculateBalance.service';

describe('calculateBalance', () => {
  const group: Group = {
    id: 1,
    title: 'Gastos',
    description: 'gastos compartidos',
    participants: [
      {
        name: 'Andrei',
        id: 2,
      },
      {
        name: 'Guille',
        id: 3,
      },
      {
        name: 'Cris',
        id: 4,
      },
    ],
    expenses: [
      {
        amountInEuro: 20,
        description: 'Cervezas',
        paidBy: {
          name: 'Andrei',
          id: 2,
        },
        paidFor: [
          {
            name: 'Andrei',
            id: 2,
          },
          {
            name: 'Guille',
            id: 3,
          },
          {
            name: 'Cris',
            id: 4,
          },
        ],
        date: DateTime.now(),
        id: 56,
      },
      {
        amountInEuro: 200,
        description: 'Cena',
        paidBy: {
          name: 'Andrei',
          id: 2,
        },
        paidFor: [
          {
            name: 'Andrei',
            id: 2,
          },
          {
            name: 'Guille',
            id: 3,
          },
          {
            name: 'Cris',
            id: 4,
          },
        ],
        date: DateTime.now(),
        id: 58,
      },
      {
        amountInEuro: 45,
        description: 'compra',
        paidBy: {
          name: 'Guille',
          id: 3,
        },
        paidFor: [
          {
            name: 'Guille',
            id: 3,
          },
          {
            name: 'Cris',
            id: 4,
          },
        ],
        date: DateTime.now(),
        id: 59,
      },
      {
        amountInEuro: 900,
        description: 'alquiler',
        paidBy: {
          name: 'Andrei',
          id: 2,
        },
        paidFor: [
          {
            name: 'Andrei',
            id: 2,
          },
          {
            name: 'Guille',
            id: 3,
          },
          {
            name: 'Cris',
            id: 4,
          },
        ],
        date: DateTime.now(),
        id: 60,
      },
      {
        amountInEuro: 130,
        description: 'Ikea',
        paidBy: {
          name: 'Cris',
          id: 4,
        },
        paidFor: [
          {
            name: 'Andrei',
            id: 2,
          },
          {
            name: 'Cris',
            id: 4,
          },
        ],
        date: DateTime.now(),
        id: 61,
      },
    ],
  };

  test('return correct balances', () => {
    const actualBalance = balanceExpenses(group);

    const expectedBalance = {
      "2": 681.6666666666666,
      "3": -350.83333333333337,
      "4": -330.83333333333337
  }

    expect(actualBalance).toEqual(expectedBalance);
  });
});
