import React from 'react';

const NotFound: React.FC = () => {
  return(
    <div>
      <h1>OOPS!!</h1>
      <h3>404: not found</h3>
    </div>
  )
}

export default NotFound