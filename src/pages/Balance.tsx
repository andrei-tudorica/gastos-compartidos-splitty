import React, { useEffect } from 'react';
import { localStorageService } from '../services/localStorage.service';
import { balanceExpenses } from '../services/calculateBalance.service';

const Balance: React.FC = () => {
  useEffect(() => {
    document.title = `Balance - Splitty`;
  });

  const group = localStorageService.getGroup();

  const balance: any = balanceExpenses(group);

  return (
    <div>
      <h1>Balance</h1>
      <div className='balance'>
        {group.participants.map((participant) => (
          <p key={participant.id}>
            <b>{participant.name}</b>:{' '}
            <span
              style={{
                color:
                  balance[participant.id] > 0
                    ? 'green'
                    : balance[participant.id] < 0
                    ? 'red'
                    : 'black',
              }}
            >
              {balance[participant.id] ? Number(balance[participant.id]).toFixed(2) : 0}
            </span>{' '}
            €
          </p>
        ))}
      </div>
    </div>
  );
};

export default Balance;

// {balance[participant.id]}
