import { User } from "./User.model.";
import { Expense } from "./Expense.model.";

export interface Group {
  id: number;
  title: string;
  description: string;
  participants: User[];
  expenses: Expense[];
}
