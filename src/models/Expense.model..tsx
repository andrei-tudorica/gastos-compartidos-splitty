// Cantidad
// Descripcción
//Persona que realizó el pago
// fecha del pago
import { DateTime } from 'luxon';
import { User } from './User.model.';

export interface Expense {
  amountInEuro: number;
  description: string;
  paidBy: User;
  paidFor: User[];
  date: DateTime;
  id: number;
}
