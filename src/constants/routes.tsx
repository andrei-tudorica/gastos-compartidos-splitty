export enum Routes {
  SPLITTY = '/',
  NEW_EXPENSE = '/new-expense',
  NEW_FRIEND = '/add-friend',
  BALANCE = '/balance'
}

