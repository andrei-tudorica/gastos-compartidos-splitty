import React from 'react';
import { Routes as ReactRoutes, Route } from 'react-router-dom';
import GroupPage from './pages/GroupPage';
import NotFound from './pages/NotFoundPage';
import { Routes } from './constants/routes';
import NewExpense from './pages/NewExpense';
import NewFriend from './pages/NewFriend';
import Balance from './pages/Balance';

function App() {
  return (
    <ReactRoutes>
      <Route path={Routes.SPLITTY} element={<GroupPage />} />
      <Route path={Routes.NEW_EXPENSE} element={<NewExpense />} />
      <Route path={Routes.NEW_FRIEND} element={<NewFriend />} />
      <Route path={Routes.BALANCE} element={<Balance />} />
      <Route path="*" element={<NotFound />} />
    </ReactRoutes>
  );
}

export default App;
