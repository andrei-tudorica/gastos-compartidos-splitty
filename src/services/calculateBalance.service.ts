import { Group } from './../models/Group.model.';

export const balanceExpenses = (group: Group) => {
  let balanceObject: any = {};
  const expenses = group.expenses;
  const participants = group.participants;

  console.log(participants);
  console.log(expenses);

  expenses.forEach((expense) => {
    const amountDivided = expense.amountInEuro / expense.paidFor.length;
    expense.paidFor.forEach((participant) => {
      const key: number = participant.id;
      if (balanceObject[key]) {
        balanceObject[key] = balanceObject[key] - amountDivided;
      } else {
        balanceObject[key] = -amountDivided;
      }
    });
    const key: number = expense.paidBy.id;
    if (balanceObject[key]) {
      balanceObject[key] = balanceObject[key] + expense.amountInEuro;
    } else {
      balanceObject[key] = expense.amountInEuro;
    }
  });
  return balanceObject;
};
