import { User } from './../models/User.model.';
import { Group } from './../models/Group.model.';
import { Expense } from './../models/Expense.model.';
import { DateTime } from 'luxon';

export const localStorageService = {

  getGroup(): Group {
    const group = localStorage.getItem('group');
    const groupObj = JSON.parse(group!);
    const expenses: Expense[] = groupObj.expenses.map((expense: any) => {
      return {
        ...expense,
        date: DateTime.fromISO(expense.date),
      };
    });

    return { ...groupObj, expenses: expenses };
  },

  saveGroup(group: Group) {
    localStorage.setItem('group', JSON.stringify(group));
  },
  
  saveNewExpense(newExpense: Expense) {
    const groupObj = this.getGroup();

    groupObj.expenses.push(newExpense);
    this.saveGroup(groupObj);
  },

  saveUser(user: User) {
    const groupObj = this.getGroup();

    groupObj.participants.push(user);
    this.saveGroup(groupObj);
  },
};
