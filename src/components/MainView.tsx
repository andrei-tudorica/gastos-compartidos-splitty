import React from 'react';
import Sidebar from './Sidebar';

interface MainViewProps {
  children: JSX.Element;
}

const MainView: React.FC<MainViewProps> = ({ children }: MainViewProps) => {
  return (
    <div className="MainView">
      <Sidebar />
      <div className="content-view">{children}</div>
    </div>
  );
};

export default MainView;
