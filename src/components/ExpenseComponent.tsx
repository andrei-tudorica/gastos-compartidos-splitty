import React from 'react';
import { Expense } from '../models/Expense.model.';

interface ExpenseComponentProps {
  expense: Expense;
}

export const ExpenseComponent: React.FC<ExpenseComponentProps> = ({
  expense,
}: ExpenseComponentProps) => {
  return (
    <div className="individual-expense">
      <div>
        <div>{expense.paidBy.name}</div>
        <div><em>{expense.description}</em></div>
      </div>
      <div>
        <div><b>{expense.amountInEuro.toFixed(2)} €</b></div>
        <div>{expense.date.toLocaleString()}</div>
      </div>
    </div>
  );
};
