import React from 'react';
import { Link } from 'react-router-dom';
import { Routes } from '../constants/routes';

const Sidebar: React.FC = () => {
  return (
    <div className="Sidebar">
      <img src='/images/Splitty.png' alt='logo'/>
      <div>
        <Link to={Routes.SPLITTY}>
          <button type="button">Mi Splitty</button>
        </Link>

        <Link to={Routes.NEW_EXPENSE}>
          <button type="button">Añade gasto</button>
        </Link>

        <Link to={Routes.NEW_FRIEND}>
          <button type="button">Añade amigos</button>
        </Link>
        <Link to={Routes.BALANCE}>
          <button type="button">Mira el Balance</button>
        </Link>
      </div>
    </div>
  );
};

export default Sidebar;
